# Using JunOS with Ansible Notes & Examples

My references for deploying to a Juniper Switch or Firewall using Ansible Core.

## Accounts

Ansible seems to handle using passwords pretty well, but ideally you are using SSH keys.
To generate an ED25519 key, use the following

```
ssh-keygen -t ed25519 -C "ansible@example.com"
```

this will then be generated in your linux account's ~/.ssh

### Account on the JunOS Appliance

I'm not going to cover automatic provisioning, so you will need to go ahead and setup the basic settings for a JunOS box that is brand new:

```
set system root-authentication plain-text-password (press enter here to type the password in)

set system login user ansible class super-user
set system login user ansible authentication ssh-ed25519 "ssh-ed25519 public-key-goes-here ansible@example.com"
set system services ssh protocol-version v2
set system services netconf ssh
```

If you are managing the appliances via the management port, you do not need to configure the zone host inbound traffic, but if you are managing it via a data port then you need, for example: 

```
set security zones security-zone SZ-MGMT interfaces ge-0/0/7.0 host-inbound-traffic system-services ssh
set security zones security-zone SZ-MGMT interfaces ge-0/0/7.0 host-inbound-traffic system-services netconf
```

## Ansible Setup

I use Fedora, or RedHat Enterprise Linux, so to install ansible core you need:

```
sudo dnf install ansible-core -y
```

Which should then give you `ansible-playbook` amongst other things.